(ns braille-plot.core
  (:require [clojure.math :as math]))

(def ^:private x' 0)
(def ^:private y' 1)
(def ^:private least-six-bit-mask  0x3f)
(def ^:private least-four-bit-mask 0x0f)

(defn- encode [codepoint]
  (let [first-six (bit-and codepoint least-six-bit-mask)
        next-six  (bit-and (bit-shift-right codepoint 6) least-six-bit-mask)
        next-four (bit-and (bit-shift-right codepoint 12) least-four-bit-mask)]
    [(bit-or 0xe0 next-four)
     (bit-or 0x80 next-six)
     (bit-or 0x80 first-six)]))

(defn- set-bit [num i value]
  (if value
    (bit-or (or num 0) (bit-shift-left 1 i))
    (bit-and (or num 0) (bit-not (bit-shift-left 1 i)))))

(defn- idiv [a b]
  (math/floor (/ a b)))

(defn- braille [byte]
  (let [bytes (encode (+ 0x2800 byte))]
    #?(:clj
       (String. (byte-array bytes))
       :cljs
       ;; TODO: maybe there's a better way?
       (let [bytes (new js/Uint8Array (clj->js bytes))]
         (.decode (new js/TextDecoder "utf-8") (clj->js bytes))))))

;; x will be 0-1 inclusive; y will be 0-3 inclusive
(defn- bit-of-subpixel [[x y]]
  (if (= y 3)
    (+ 6 x)
    (+ (* 3 x) y)))

(defn- set-subpixel [num subpixel value]
  (set-bit num (bit-of-subpixel subpixel) value))

;; returns width and height in "subpixels"
(defn- bounds [{:keys [width height]}]
  [(* 2 width) (* 4 height)])

(defn- span [axis p0 p1]
  (- (get p1 axis) (get p0 axis)))

(defn- sign [x]
  (cond (< x 0) -1
        (> x 0) 1
        (= x 0) 0))

(defn- make-vec2 [major' major minor]
  (case major'
    0 [major minor]
    1 [minor major]
    (throw (ex-info "invalid" {}))))

;; Public API

(defn draw-point
  ([canvas point]
   (draw-point canvas point true))
  ([{:keys [width height] :as canvas} [x y] value]
   (let [x (math/round (float x))
         y (math/round (float y))
         [pixel-x pixel-y] [(idiv x 2) (idiv y 4)]]
     (if (or (< pixel-x 0) (< pixel-y 0)
             (>= pixel-x width) (>= pixel-y height))
       canvas
       (let [pixel-ix (int (+ (* pixel-y width) pixel-x))
             subpixel [(mod x 2) (mod y 4)]]
         (update-in canvas [:pixels pixel-ix] #(set-subpixel % subpixel value)))))))

(defn draw-line [canvas start end]
  (let [x-span (span x' start end)
        y-span (span y' start end)
        [major' minor']
        (if (< (abs y-span) (abs x-span))
          [x' y']
          [y' x'])
        [start end]
        (if (< (start major') (end major'))
          [start end]
          [end start])
        minor-step (sign (- (end minor') (start minor')))
        run (- (end major') (start major'))
        rise (abs (- (end minor') (start minor')))]
    (loop [[major & majors] (range (start major') (end major'))
           canvas canvas
           minor (start minor')
           err (- (* 2 rise) run)]
      (if major
        (recur majors
               (draw-point canvas (make-vec2 major' major minor))
               (+ minor (if (> err 0) minor-step 0))
               (+ err (* 2 rise) (if (> err 0) (- (* 2 run)) 0)))
        canvas))))

(defn draw-axies [canvas]
  (let [[w h] (bounds canvas)]
    (-> canvas
        (draw-line [(/ w 2) 0] [(/ w 2) h])
        (draw-line [0 (/ h 2)] [w (/ h 2)]))))

(defn canvas
  [& {:keys [width height x-scale y-scale]}]
  (let [[width height]
        (cond (and width height) [width height]
              width [width (/ width 2)]
              height [(* height 2) height]
              :else [40 20])
        [x-scale y-scale]
        (cond (and x-scale y-scale) [x-scale y-scale]
              x-scale [x-scale x-scale]
              y-scale [y-scale y-scale]
              :else [1 1])]
    {:width width
     :height height
     :x-scale x-scale
     :y-scale y-scale
     :pixels (vec (int-array (* width height) 0))}))

(defn print-canvas [{:keys [width height pixels]}]
  (dotimes [y height]
    (dotimes [x width]
      (let [i (+ (* y width) x)]
        (print (braille (nth pixels i)))))
    (println)))

(defn plot
  ([f] (plot f (canvas)))
  ([f {:keys [x-scale y-scale]
       :as canvas}]
   (let [[w h] (bounds canvas)
         y-scale (* y-scale (/ (+ h 1) h))]
     (loop [[x & xs] (range w)
            current-point nil
            canvas canvas]
       (if x
         (let [x (- (/ x (- w 1)) 0.5)
               y (f (* x 2 x-scale))]
           (if (and (number? y) (not (NaN? y)))
             (let [y (/ y y-scale -2)
                   p [(* (+ x 0.5) (- w 1))
                      (* (+ y 0.5) h)]]
               (recur xs p
                      (cond-> canvas
                        current-point (draw-line current-point p))))
             (recur xs nil canvas)))
         canvas)))))
